{ pkgs ? (
    let
      inherit (builtins) fetchTree fromJSON readFile;
      inherit ((fromJSON (readFile ./flake.lock)).nodes) nixpkgs gomod2nix;
    in
    import (fetchTree nixpkgs.locked) {
      overlays = [
        (import "${fetchTree gomod2nix.locked}/overlay.nix")
      ];
    }
  )
}:

pkgs.buildGoApplication {
  pname = "tigris";
  version = "0.1";
  pwd = ./.;
  src = ./.;
  modules = ./gomod2nix.toml;
    buildPhase = ''
    go build -o tigris ./cmd/tigris-server/main.go
  '';

  # omit steps in checkPhase
  checkPhase = "";

  # install the binary in ./daggy/cmd/daggy-server
  installPhase = ''
    mkdir -p $out/bin
    cp tigris $out/bin
  '';
}
