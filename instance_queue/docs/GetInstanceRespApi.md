# {{classname}}

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetInstanceId**](GetInstanceRespApi.md#GetInstanceId) | **Get** /get_instance | 

# **GetInstanceId**
> GetInstanceResp GetInstanceId(ctx, )


Get instance ID from queue  Retrieves the next available EC2 instance ID from the queue.

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**GetInstanceResp**](GetInstanceResp.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

