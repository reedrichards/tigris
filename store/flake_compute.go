package store

import (
	"log"
	"pbin/models"
	"sync"

	"github.com/pkg/errors"
	"github.com/samber/lo"
	"go.uber.org/multierr"

	"gorm.io/datatypes"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type FlakeCompute interface {
	Put(ModelFlakeCompute) error
	Get(ID string) (*ModelFlakeCompute, error)
	Delete(ID string) error
}

type MemoryFlakeCompute struct {
	FlakeComputes map[string]ModelFlakeCompute
	m             sync.RWMutex
}

func (m *MemoryFlakeCompute) Put(flakeCompute ModelFlakeCompute) error {
	m.m.Lock()
	defer m.m.Unlock()
	m.FlakeComputes[flakeCompute.ID] = flakeCompute
	return nil
}

func (m *MemoryFlakeCompute) Get(ID string) (*ModelFlakeCompute, error) {
	m.m.RLock()
	defer m.m.RUnlock()
	flakeCompute, ok := m.FlakeComputes[ID]
	if !ok {
		return nil, nil
	}
	return &flakeCompute, nil
}

func (m *MemoryFlakeCompute) Delete(ID string) error {
	m.m.Lock()
	defer m.m.Unlock()
	delete(m.FlakeComputes, ID)
	return nil
}

func NewMemoryFlakeCompute() *MemoryFlakeCompute {
	return &MemoryFlakeCompute{
		FlakeComputes: make(map[string]ModelFlakeCompute),
	}
}

type ModelFlakeCompute struct {
	models.FlakeCompute
	DeploymentLogs datatypes.JSON
}

func (m *ModelFlakeCompute) GetDeploymentLogs() (*models.DeploymentLogs, error) {
	logs := &models.DeploymentLogs{}
	if m.DeploymentLogs == nil {
		return nil, nil
	}
	err := logs.UnmarshalBinary(m.DeploymentLogs)
	return logs, err
}

func reduceLogItems(items []*models.DeploymentLogItem, f func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem) []*models.DeploymentLogItem {
	return lo.Reduce(items, f, make([]*models.DeploymentLogItem, 0))
}

func (m *ModelFlakeCompute) ReduceDeploymentLogs(
	f func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, _ int) []*models.DeploymentLogItem,
) error {
	items, err := m.GetDeploymentLogs()
	if err != nil {
		return err
	}
	items.Items = reduceLogItems(items.Items, f)
	dlBytes, err := items.MarshalBinary()
	if err != nil {
		return err
	}
	m.DeploymentLogs = datatypes.JSON(dlBytes)
	return nil

}

type SQLiteFlakeCompute struct {
	ModelFlakeCompute
	db *gorm.DB
}

func NewSQLiteFlakeCompute(
	dbPath string,
) (*SQLiteFlakeCompute, error) {
	db, err := gorm.Open(sqlite.Open(dbPath), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	db.AutoMigrate(&SQLiteFlakeCompute{})
	return &SQLiteFlakeCompute{
		db: db,
	}, nil
}

func (s *SQLiteFlakeCompute) Put(flakeCompute ModelFlakeCompute) error {
	// check if exists
	existing, err := s.Get(flakeCompute.ID)
	if err != nil {
		return err
	}
	if existing != nil {
		tx := s.db.Delete(&s, "id = ?", flakeCompute.ID)
		if tx.Error != nil {
			log.Println(err)
		}
	}
	s.FlakeCompute = flakeCompute.FlakeCompute
	s.DeploymentLogs = flakeCompute.DeploymentLogs
	tx := s.db.Create(&s)
	return tx.Error
}

func (s *SQLiteFlakeCompute) Get(ID string) (*ModelFlakeCompute, error) {
	var flakeCompute SQLiteFlakeCompute
	s.db.First(&flakeCompute, "id = ?", ID)
	return &flakeCompute.ModelFlakeCompute, nil
}

func (s *SQLiteFlakeCompute) Delete(ID string) error {
	s.db.Delete(&s, "id = ?", ID)
	return nil
}

type FlakeFileSqlite struct {
	models.FlakeFile
	db             *gorm.DB
	FlakeComputeID string
	secretKey      *[32]byte
}

type FlakeFileStore interface {
	PutFile(string, models.FlakeFile) error
	GetFile(ID string) (*models.FlakeFile, error)
	DeleteFile(ID string) error
	GetFiles(flakeComputeID string) ([]*models.FlakeFile, error)
}

// FlakeFileStore implementation using SQLite
func (s *FlakeFileSqlite) PutFile(flakeComputeID string, flakeFile models.FlakeFile) error {
	// check if exists
	existing, _ := s.GetFile(flakeFile.ID)
	// if err != nil {
	// 	return err
	// }
	if existing != nil {
		s.db.Delete(&s, "id = ?", flakeFile.ID)
	}

	err := flakeFile.Encrypt(s.secretKey)
	if err != nil {
		return err
	}

	s.FlakeFile = flakeFile
	s.FlakeComputeID = flakeComputeID
	s.db.Create(&s)
	return nil
}

func (s *FlakeFileSqlite) GetFile(ID string) (*models.FlakeFile, error) {
	var flakeFile FlakeFileSqlite
	s.db.First(&flakeFile, "id = ?", ID)
	err := flakeFile.Decrypt(s.secretKey)
	return &flakeFile.FlakeFile, err
}

func (s *FlakeFileSqlite) DeleteFile(ID string) error {
	var flakeFile FlakeFileSqlite
	tx := s.db.Delete(&flakeFile, "id = ?", ID)
	return tx.Error
}

func (s *FlakeFileSqlite) GetFiles(flakeComputeID string) ([]*models.FlakeFile, error) {
	var flakeFiles []FlakeFileSqlite
	s.db.Find(&flakeFiles, "flake_compute_id = ?", flakeComputeID)
	mFlakeFiles := lo.Map(flakeFiles,
		func(f FlakeFileSqlite, _ int) *models.FlakeFile {
			return &f.FlakeFile
		})

	return mFlakeFiles, lo.Reduce(
		mFlakeFiles,
		func(acc error, f *models.FlakeFile, _ int) error {
			return multierr.Append(
				acc,
				errors.Wrap(
					f.Decrypt(s.secretKey),
					"error decrypting flake file",
				),
			)
		},
		nil,
	)

}

func NewSqliteFlakeFile(
	dbPath string, secretkey *[32]byte,
) (*FlakeFileSqlite, error) {
	db, err := gorm.Open(sqlite.Open(dbPath), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	db.AutoMigrate(&FlakeFileSqlite{})
	return &FlakeFileSqlite{
		db:        db,
		secretKey: secretkey,
	}, nil
}

type FlakeStore interface {
	FlakeCompute
	FlakeFileStore
}

// eww this is bad, refactor me please
func NewSQLiteFlakeStore(
	dbPath string, secretkey *[32]byte,
) (FlakeStore, error) {
	flakeCompute, err := NewSQLiteFlakeCompute(dbPath)
	if err != nil {
		return nil, err
	}
	flakeFile, err := NewSqliteFlakeFile(dbPath, secretkey)
	if err != nil {
		return nil, err
	}
	return &SQLiteFlakeStore{
		*flakeCompute,
		*flakeFile,
	}, nil
}

type SQLiteFlakeStore struct {
	SQLiteFlakeCompute
	FlakeFileSqlite
}
