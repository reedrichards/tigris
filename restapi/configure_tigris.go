// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/route53"
	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/samber/lo"
	"github.com/samber/mo"
	"go.uber.org/multierr"
	"go.uber.org/zap"
	"golang.org/x/crypto/ssh"

	iq "pbin/instance_queue"
	"pbin/models"
	"pbin/restapi/operations"
	"pbin/store"
)

//go:generate swagger generate server --target ../../nercel --name Tigris --spec ../swagger.yml --principal interface{}

func configureFlags(api *operations.TigrisAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.TigrisAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	logger, _ := zap.NewProduction()
	sugar := logger.Sugar()
	dbPath := os.Getenv("DB_PATH")
	if dbPath == "" {
		dbPath = "flake_compute.db"
	}

	secretKeyPath := os.Getenv("SECRET_KEY_PATH")
	if secretKeyPath == "" {
		secretKeyPath = "secret.key"
	}

	secretKeyContent, err := os.ReadFile(secretKeyPath)
	if err != nil {
		log.Fatal(err)
	}

	secretKey, err := base64.StdEncoding.DecodeString(string(secretKeyContent))
	if err != nil {
		log.Fatal(err)
	}
	fcstore, err := store.NewSQLiteFlakeStore(dbPath, (*[32]byte)(secretKey))
	if err != nil {
		log.Fatal(err)
	}

	redisURI := os.Getenv("REDIS_URI")
	redisURI = mo.EmptyableToOption(redisURI).OrElse("0.0.0.0:6379")

	// Initialize Asynq client
	redisClientOpt := asynq.RedisClientOpt{Addr: redisURI}
	client := asynq.NewClient(redisClientOpt)
	// defer client.Close()

	sess, err := session.NewSession(
		&aws.Config{
			Region: aws.String("us-west-1"),
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	// Create an EC2 service client
	svc := ec2.New(sess)

	route53svc := route53.New(sess)

	go myBackgroundWorker(redisClientOpt, fcstore, svc, route53svc)

	iqClient := iq.NewAPIClient(
		&iq.Configuration{
			// 0.0.0.0:8000
			// Host: "0.0.0.0:8000",
			// Scheme: "http",
			BasePath:      "http://0.0.0.0:8000",
			DefaultHeader: make(map[string]string),
			UserAgent:     "Swagger-Codegen/1.0.0/go",
		},
	)

	nffn := getNewFlakeFunc(
		fcstore,
		svc,
		client,
		iqClient,
	)

	// Applies when the "x-token" header is set
	api.KeyAuth = func(token string) (*models.Principal, error) {
		authToken := os.Getenv("AUTH_TOKEN")
		if authToken == "" {
			return nil, errors.New(401, "server auth token not set")
		}
		if token == authToken {
			prin := models.Principal(token)
			return &prin, nil
		}
		return nil, errors.New(401, "incorrect api key auth")
	}

	api.FlakeHandler = operations.FlakeHandlerFunc(
		func(
			params operations.FlakeParams,
			principal *models.Principal,
		) middleware.Responder {
			if params.FlakeURL == nil {
				return middleware.Error(400, fmt.Errorf("flake_url is required"))
			}

			fmt.Printf("flake_url: %s\n", *params.FlakeURL.FlakeURL)

			id, err := nffn(
				*params.FlakeURL,
			)
			if err != nil {
				sugar.Errorw("failed_to_create_flake_compute", "error", err)
				return middleware.Error(500, err)
			}

			if id == nil {
				return middleware.Error(500, fmt.Errorf("failed to create flake compute"))
			}

			fc, err := fcstore.Get(*id)

			if err != nil {
				sugar.Errorw("failed_to_create_flake_compute", "error", err)
				return middleware.Error(500, err)
			}

			return operations.NewFlakeOK().WithPayload(
				&models.FlakeComputeResponse{
					FlakeCompute: &fc.FlakeCompute,
				},
			)

		})

	api.GetflakeHandler = operations.GetflakeHandlerFunc(
		func(params operations.GetflakeParams, principal *models.Principal) middleware.Responder {
			fc, err := fcstore.Get(params.ID)
			if err != nil {
				return middleware.Error(500, err)
			}
			deploymentLogs, err := fc.GetDeploymentLogs()
			if err != nil {
				sugar.Errorw(
					"error_getting_logs",
					"err", err.Error(),
				)
			}

			return operations.NewGetflakeOK().WithPayload(
				&models.FlakeComputeResponse{
					FlakeCompute: &fc.FlakeCompute,
					Logs:         deploymentLogs,
				},
			)
		})

	api.DeleteflakeHandler = operations.DeleteflakeHandlerFunc(func(params operations.DeleteflakeParams, principal *models.Principal) middleware.Responder {

		flake, err := fcstore.Get(params.ID)
		if err != nil {
			return middleware.Error(500, err)
		}
		if flake == nil {
			return middleware.Error(404, fmt.Errorf("flake compute not found"))
		}
		err = fcstore.Delete(params.ID)
		if err != nil {
			return middleware.Error(500, err)
		}
		sess, err := session.NewSession(
			&aws.Config{
				Region: aws.String("us-west-1"),
			},
		)
		if err != nil {
			return middleware.Error(500, err)
		}

		// Create an EC2 service client
		svc := ec2.New(sess)
		// delete instance id
		_, err = svc.TerminateInstances(&ec2.TerminateInstancesInput{
			InstanceIds: aws.StringSlice([]string{flake.InstanceID}),
		})
		if err != nil {
			return middleware.Error(500, err)
		}
		return operations.NewDeleteflakeOK()
	})

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

func generateRandomString() (string, error) {
	b := make([]byte, 4) // Generate bytes to get at least 6 characters after base64 encoding
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	str := base64.URLEncoding.EncodeToString(b)
	sixChars := str[:6] // Return the first 6 characters
	lowercase := strings.ToLower(sixChars)
	return lowercase, nil
}

func createARecord(
	fc models.FlakeCompute,
	route53svc *route53.Route53,
) error {
	hostedZoneId := "Z03309493AGZOVY2IU47X" // Replace with your hosted zone ID
	domainName := fc.Domain
	ipAddress := fc.IP

	// Prepare the change batch request
	input := &route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &route53.ChangeBatch{
			Changes: []*route53.Change{
				{
					Action: aws.String("UPSERT"),
					ResourceRecordSet: &route53.ResourceRecordSet{
						Name: aws.String(domainName),
						Type: aws.String("A"),
						TTL:  aws.Int64(300),
						ResourceRecords: []*route53.ResourceRecord{
							{
								Value: aws.String(ipAddress),
							},
						},
					},
				},
			},
		},
		HostedZoneId: aws.String(hostedZoneId),
	}

	// Attempt to update the DNS record
	_, err := route53svc.ChangeResourceRecordSets(input)
	if err != nil {
		fmt.Println("Error updating DNS record: ", err)
		return err
	}
	return nil

}

func getNewFlakeFunc(
	fcstore store.FlakeStore,
	svc *ec2.EC2,
	client *asynq.Client,
	iqClient *iq.APIClient,
) func(models.FlakeInput) (*string, error) {
	return func(fi models.FlakeInput) (*string, error) {
		fc := models.FlakeCompute{
			ID:       uuid.New().String(),
			FlakeURL: *fi.FlakeURL,
			Status:   "creating instance\n",

			Domain: fmt.Sprintf(
				"%s.app.flakery.xyz",
				fi.SubdomainPrefix,
			),
		}
		dl := &models.DeploymentLogs{
			Items: []*models.DeploymentLogItem{
				{
					Icon:    "i-heroicons-computer",
					Label:   "Creating Instance",
					Loading: true,
				},
				{
					Icon:     "i-heroicons-phone",
					Label:    "Establish Connection",
					Disabled: true,
				},
				{
					Icon:     "i-heroicons-document-arrow-up",
					Label:    "Seed Filesystem",
					Disabled: true,
				},
				{
					Icon:     "i-heroicons-cube-transparent",
					Label:    "Apply Flake",
					Disabled: true,
				},
			},
		}
		now := time.Now()
		dlBytes, err := dl.MarshalBinary()
		if err != nil {
			return &fc.ID, err
		}
		storeFC := store.ModelFlakeCompute{
			FlakeCompute:   fc,
			DeploymentLogs: dlBytes,
		}
		err = fcstore.Put(storeFC)
		if err != nil {
			return &fc.ID, err
		}

		for _, file := range fi.Files {
			if file == nil {
				continue
			}
			file.ID = uuid.NewString() // hack to support redeploy
			err := fcstore.PutFile(fc.ID, *file)
			if err != nil {
				return &fc.ID, err
			}
		}

		// Define the launch template ID and version
		launchTemplateId := "lt-0d7b76529ceabcb50"
		launchTemplateVersion := "$Latest" // specify version or use "$Default" or "$Latest"

		// Create the input configuration
		input := &ec2.RunInstancesInput{
			LaunchTemplate: &ec2.LaunchTemplateSpecification{
				LaunchTemplateId: aws.String(launchTemplateId),
				Version:          aws.String(launchTemplateVersion),
			},
			MaxCount:     aws.Int64(1),
			MinCount:     aws.Int64(1),
			InstanceType: fi.GetInstanceType(),
			TagSpecifications: []*ec2.TagSpecification{
				{
					ResourceType: aws.String("instance"),
					Tags: []*ec2.Tag{
						{
							Key:   aws.String("Name"),
							Value: aws.String(mo.EmptyableToOption(os.Getenv("INSTANCE_NAME")).OrElse("nix")),
						},
					},
				},
			},
		}

		instanceId, err := getInstanceIDFromQueue(iqClient)

		if err != nil {
			// Create the instance
			result, err := svc.RunInstances(input)
			if err != nil {
				return &fc.ID, err
			}

			// allocat public ip
			// todo wait for instance to be ready
			instanceId = result.Instances[0].InstanceId
		}

		fc.Status = fc.Status + "instance created\n"
		storeFC.ReduceDeploymentLogs(
			func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, _ int) []*models.DeploymentLogItem {
				if item.Label == "Creating Instance" {
					item.Label = "Instance Created"
					item.Icon = "i-heroicons-check"
					item.Loading = false
					item.Content = fmt.Sprintf(
						"Created instance in %s",
						time.Since(now).String(),
					)
				}
				if item.Label == "Establish Connection" {
					item.Disabled = false
					item.Label = "Establishing Connection"
					item.Loading = true
				}
				return append(acc, item)
			},
		)

		fc.InstanceID = *instanceId
		err = fcstore.Put(
			store.ModelFlakeCompute{
				FlakeCompute:   fc,
				DeploymentLogs: storeFC.DeploymentLogs,
			},
		)
		if err != nil {
			return &fc.ID, err
		}

		payload, err := json.Marshal(CreateInstancePayload{
			ID: fc.ID,
		})
		if err != nil {
			return &fc.ID, err
		}

		_, err = client.Enqueue(
			asynq.NewTask(TaskType, payload),
			asynq.MaxRetry(0),
			asynq.Deadline(time.Now().Add(time.Minute*10)),
		)
		if err != nil {
			log.Fatalf("Could not enqueue task: %v", err)
		}

		return &fc.ID, err
	}
}

func myBackgroundWorker(
	redisClientOpt asynq.RedisClientOpt,
	store store.FlakeStore,
	svc *ec2.EC2,

	route53svc *route53.Route53,

) {

	// Initialize Asynq server
	srv := asynq.NewServer(
		redisClientOpt,
		asynq.Config{},
	)

	// Register task handler
	mux := asynq.NewServeMux()
	mux.HandleFunc(TaskType, getCreateInstanceTaskHandler(
		svc,
		store,
		route53svc,
	))

	// Start the server
	if err := srv.Run(mux); err != nil {
		log.Fatalf("Could not run Asynq server: %v", err)
	}

}

func getCreateInstanceTaskHandler(
	svc *ec2.EC2,
	fcstore store.FlakeStore,
	route53svc *route53.Route53,
) func(ctx context.Context, t *asynq.Task) error {
	return func(ctx context.Context, t *asynq.Task) error {
		var p CreateInstancePayload
		if err := json.Unmarshal(t.Payload(), &p); err != nil {
			return fmt.Errorf("json.Unmarshal failed: %v", err)
		}

		// Process the job
		return processJob(p.ID, svc, fcstore, route53svc)
	}
}

func processJob(
	id string,
	svc *ec2.EC2,
	fcstore store.FlakeStore,
	route53svc *route53.Route53,

) error {
	fmt.Println("processing job", id)
	now := time.Now()

	fc, err := fcstore.Get(id)
	if err != nil {
		return err
	}
	if fc == nil {
		return fmt.Errorf("flake compute not found")
	}
	instanceId := fc.InstanceID
	flake_url := fc.FlakeURL

	fc.Status = fc.Status + "waiting for instance to be ready\n"
	waiting := time.Now()
	err = fc.ReduceDeploymentLogs(
		func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
			if item.Label == "Establishing Connection" {
				item.Content = "waiting for instance to be ready\n"
			}
			return append(acc, item)
		},
	)
	if err != nil {
		return err
	}
	err = fcstore.Put(*fc)
	if err != nil {
		return err
	}
	// Wait for the instance to be in a running state
	err = svc.WaitUntilInstanceRunning(&ec2.DescribeInstancesInput{
		InstanceIds: aws.StringSlice([]string{instanceId}),
	})
	if err != nil {
		err2 := fc.ReduceDeploymentLogs(
			func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
				if item.Label == "Establishing Connection" {
					item.Content = item.Content + fmt.Sprintf("failed to wait for instance to be ready after %s\n", time.Since(waiting).String())
					item.Error = true
				}
				return append(acc, item)
			},
		)
		return multierr.Combine(err, fcstore.Put(*fc), err2)
	}
	// ssh into instance using key.pem and run flake_url
	// ssh -i key.pem ubuntu@public_ip flake_url
	// get instance public ip
	fmt.Println("get instance public ip")

	instance, err := svc.DescribeInstances(&ec2.DescribeInstancesInput{
		InstanceIds: aws.StringSlice([]string{instanceId}),
	})
	if err != nil {
		err2 := fc.ReduceDeploymentLogs(
			func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, _ int) []*models.DeploymentLogItem {
				if item.Label == "Establishing Connection" {
					item.Content = item.Content + "failed to get instance public ip\n"
					item.Error = true
				}
				return append(acc, item)
			},
		)

		return multierr.Combine(err, fcstore.Put(*fc), err2)
	}

	fc.Status = fc.Status + "establishing connection\n"
	err = fc.ReduceDeploymentLogs(
		// add got ip address to content
		func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
			if item.Label == "Establishing Connection" {
				item.Content = item.Content +
					fmt.Sprintf("got public ip %s\n", *instance.Reservations[0].Instances[0].PublicIpAddress) +
					fmt.Sprintln("testing connection")
			}
			return append(acc, item)
		},
	)
	if err != nil {
		return err
	}

	ipAddr := *instance.Reservations[0].Instances[0].PublicIpAddress
	fc.IP = ipAddr
	// set logs
	fmt.Println("set logs")
	err = fcstore.Put(*fc)
	if err != nil {
		return err
	}

	err = createARecord(
		fc.FlakeCompute,
		route53svc,
	)
	if err != nil {
		return err
	}

	i := 0
	var e error
	for i < 100 {
		fmt.Printf("try %d, %s\n", i, instanceId)
		e = testSSHConnection(ipAddr, "root")
		if e != nil {
			i++
			time.Sleep(2 * time.Second)
			continue
		}
		break
	}
	if e != nil {
		err2 := fc.ReduceDeploymentLogs(
			func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, _ int) []*models.DeploymentLogItem {
				if item.Label == "Establishing Connection" {
					item.Content = item.Content + "failed to establish connection\n"
					item.Error = true
				}
				return append(acc, item)
			},
		)
		return multierr.Combine(e, fcstore.Put(*fc), err2)
	}
	fc.Status = fc.Status + "connection established\n"

	// add all files to instance
	fc.Status = fc.Status + "seeding filesystem\n"
	err = fc.ReduceDeploymentLogs(func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
		if item.Label == "Seed Filesystem" {
			item.Disabled = false
			item.Label = "Seeding Filesystem"
			item.Loading = true
		}
		if item.Label == "Establishing Connection" {
			item.Label = "Connection Established"
			item.Loading = false
			item.Content = item.Content + fmt.Sprintf(
				"Established Connection in %s",
				time.Since(now).String(),
			)
			item.Icon = "i-heroicons-check"
		}
		return append(acc, item)

	})
	if err != nil {
		return err
	}
	err = fcstore.Put(store.ModelFlakeCompute{
		FlakeCompute:   fc.FlakeCompute,
		DeploymentLogs: fc.DeploymentLogs,
	})

	if err != nil {
		return err
	}
	now = time.Now()

	files, err := fcstore.GetFiles(fc.ID)
	if err != nil {
		return err
	}
	if files == nil {
		files = make([]*models.FlakeFile, 0)
	}

	files = append(files, &models.FlakeFile{
		Path:    "/metadata/flakery-id",
		Content: fc.ID,
	})

	files = append(files, &models.FlakeFile{
		Path:    "/metadata/flakery-domain",
		Content: fc.Domain,
	})

	for _, file := range files {
		err = writeFileToInstance(ipAddr, "root", *file)
		if err != nil {
			fc.Status = fc.Status + fmt.Sprintf("failed to write file %s\n", file.Path)
			fc.Status = fc.Status + fmt.Sprintf("error: %s\n", err)
			err2 := fc.ReduceDeploymentLogs(
				func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, _ int) []*models.DeploymentLogItem {
					if item.Label == "Seeding Filesystem" {
						item.Label = "Filesystem Seeding Failed"
						item.Loading = false
						item.Icon = "i-heroicons-x-mark"
						item.Content = item.Content + fmt.Sprintf("failed to write file %s\n", file.Path)
						item.Error = true
					}
					return append(acc, item)
				},
			)
			return multierr.Combine(err, fcstore.Put(*fc), err2)
		}
	}

	now = time.Now()

	// run flake_url
	fmt.Println("run flake_url")
	fc.Status = fc.Status + "applying flake\n"
	err = fc.ReduceDeploymentLogs(
		func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
			if item.Label == "Apply Flake" {
				item.Disabled = false
				item.Label = "Applying Flake"
				item.Loading = true
			}
			if item.Label == "Seeding Filesystem" {
				item.Label = "Filesystem Seeded"
				item.Loading = false
				item.Content = item.Content +
					fmt.Sprintf("seeded files at paths: \n%s\n", strings.Join(
						lo.Map(files, func(f *models.FlakeFile, _ int) string {
							return f.Path
						}),
						"\n",
					)) +
					fmt.Sprintf(
						"\nSeeded Filesystem in %s",
						time.Since(now).String(),
					)
				item.Icon = "i-heroicons-check"

			}
			return append(acc, item)
		},
	)
	if err != nil {
		return err
	}

	err = fcstore.Put(store.ModelFlakeCompute{
		FlakeCompute:   fc.FlakeCompute,
		DeploymentLogs: fc.DeploymentLogs,
	})
	if err != nil {
		return err
	}

	err = sshAndRunNixFlake(ipAddr, "root", flake_url, fc, fcstore)

	if err != nil {
		return err
	}

	fc.Status = fc.Status + "flake applied successfully\n"
	err = fc.ReduceDeploymentLogs(
		func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
			if item.Label == "Applying Flake" {
				item.Label = "Flake Applied"
				item.Icon = "i-heroicons-check"

			}
			return append(acc, item)
		},
	)

	return err

}

func getInstanceIDFromQueue(
	client *iq.APIClient,
) (*string, error) {
	s, _, err := client.GetInstanceRespApi.GetInstanceId(context.Background())

	if err != nil {
		return nil, err
	}

	return &s.InstanceId, nil
}

func writeFileToInstance(ip, user string, file models.FlakeFile) error {
	key, err := getKey()
	if err != nil {
		return err
	}
	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	// Create client config
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Connect to ssh server
	conn, err := ssh.Dial("tcp", ip+":22", config)
	if err != nil {
		return err
	}
	defer conn.Close()

	session, err := conn.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()
	session.RequestPty("dumb", 80, 80, ssh.TerminalModes{
		ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	})

	buf := new(bytes.Buffer)
	session.Stdout = buf
	session.Stderr = buf

	b64encoded := base64.StdEncoding.EncodeToString([]byte(file.Content))

	// todo is there a better way to do this?
	// get dirpath to file to run mkdir -p
	dirpath := filepath.Dir(file.Path)
	err = session.Run(fmt.Sprintf("mkdir -p %s && ", dirpath) + fmt.Sprintf("echo %s | base64 -d > %s", b64encoded, file.Path))

	if err != nil {
		return err
	}
	return nil
}

func getKey() ([]byte, error) {
	keypath := os.Getenv("KEY_PATH")
	if keypath == "" {
		// read ./key.pem
		keypath = "./key.pem"
	}
	f, err := os.Open(keypath)

	if err != nil {
		return nil, err
	}
	defer f.Close()

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(f)
	return buf.Bytes(), err

}
func sshAndRunNixFlake(
	ip, user, flake_url string,
	fc *store.ModelFlakeCompute,
	fcstore store.FlakeStore,
) error {
	// Create the Signer for this private key.
	key, err := getKey()
	if err != nil {
		return err
	}
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	// Create client config
	config := &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Connect to ssh server
	conn, err := ssh.Dial("tcp", ip+":22", config)
	if err != nil {
		return err
	}
	defer conn.Close()

	session, err := conn.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	// Request a pseudo terminal
	session.RequestPty("dumb", 80, 40, ssh.TerminalModes{
		ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	})

	// Set up session output to be continuously logged
	stdout, err := session.StdoutPipe()
	if err != nil {
		return err
	}

	stderr, err := session.StderrPipe()
	if err != nil {
		return err
	}

	// Start the command asynchronously
	err = session.Start(fmt.Sprintf("nixos-rebuild switch -L --impure --flake '%s'", flake_url))
	if err != nil {
		return err
	}

	// Create a multi-reader to handle both stdout and stderr
	multi := io.MultiReader(stdout, stderr)
	// go io.Copy(os.Stdout, multi) // Continuously log the output to os.Stdout
	// read from multi, and for each line, print output
	go func(multi io.Reader) {
		buf := make([]byte, 1024)
		for {
			n, err := multi.Read(buf)
			if err != nil {
				if err == io.EOF {
					break
				}
				log.Fatal(err)
			}
			fmt.Print(string(buf[:n]))
			err2 := fc.ReduceDeploymentLogs(
				func(acc []*models.DeploymentLogItem, item *models.DeploymentLogItem, index int) []*models.DeploymentLogItem {
					if strings.Contains(item.Label, "Flake") {
						item.Content = item.Content + string(buf[:n])
					}
					return append(acc, item)
				},
			)
			if err2 != nil {
				log.Fatal(err2) // todo not fatal
			}
			err = fcstore.Put(*fc)
			if err != nil {
				log.Fatal(err) // todo not fatal
			}

		}
	}(multi)

	// Wait for the session to finish
	err = session.Wait()
	return err
}

func testSSHConnection(ip, user string) error {
	key, err := getKey()
	if err != nil {
		return err
	}
	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	// Create client config
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Connect to ssh server
	conn, err := ssh.Dial("tcp", ip+":22", config)
	if err != nil {
		return err
	}
	defer conn.Close()

	session, err := conn.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	return nil

}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}

const (
	TaskType = "create-instance"
)

// Define a task payload structure
type CreateInstancePayload struct {
	ID string `json:"id"`
}
