// Code generated by go-swagger; DO NOT EDIT.

package restapi

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"
)

var (
	// SwaggerJSON embedded version of the swagger document used at generation time
	SwaggerJSON json.RawMessage
	// FlatSwaggerJSON embedded flattened version of the swagger document used at generation time
	FlatSwaggerJSON json.RawMessage
)

func init() {
	SwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "provide a nix flake and tigris provisions a compute environment and applies the flake to it",
    "title": "tigris",
    "version": "1.0.0"
  },
  "basePath": "/",
  "paths": {
    "/flake": {
      "post": {
        "description": "create a flake deployment",
        "summary": "create a flake deployment",
        "operationId": "flake",
        "parameters": [
          {
            "description": "flake url",
            "name": "flakeURL",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/FlakeInput"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "flake computed",
            "schema": {
              "$ref": "#/definitions/FlakeComputeResponse"
            }
          }
        }
      }
    },
    "/flake/{id}": {
      "get": {
        "description": "get a flake deployment",
        "summary": "get a flake deployment",
        "operationId": "getflake",
        "parameters": [
          {
            "type": "string",
            "description": "flake id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "flake computed",
            "schema": {
              "$ref": "#/definitions/FlakeComputeResponse"
            }
          }
        }
      },
      "delete": {
        "description": "delete a flake deployment",
        "summary": "delete a flake deployment",
        "operationId": "deleteflake",
        "parameters": [
          {
            "type": "string",
            "description": "flake id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "flake deleted",
            "schema": {
              "$ref": "#/definitions/FlakeCompute"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "DeploymentLogItem": {
      "type": "object",
      "properties": {
        "completed": {
          "type": "boolean"
        },
        "content": {
          "type": "string"
        },
        "disabled": {
          "type": "boolean"
        },
        "error": {
          "type": "boolean"
        },
        "icon": {
          "type": "string"
        },
        "label": {
          "type": "string"
        },
        "loading": {
          "type": "boolean"
        }
      }
    },
    "DeploymentLogs": {
      "type": "object",
      "properties": {
        "completed": {
          "type": "boolean"
        },
        "items": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DeploymentLogItem"
          }
        }
      }
    },
    "FlakeCompute": {
      "type": "object",
      "properties": {
        "domain": {
          "type": "string"
        },
        "flakeURL": {
          "type": "string"
        },
        "id": {
          "type": "string"
        },
        "instanceID": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "isTemplate": {
          "type": "boolean"
        },
        "logs": {
          "type": "string"
        },
        "status": {
          "type": "string"
        },
        "template": {
          "type": "string"
        }
      }
    },
    "FlakeComputeResponse": {
      "type": "object",
      "properties": {
        "flakeCompute": {
          "$ref": "#/definitions/FlakeCompute"
        },
        "logs": {
          "$ref": "#/definitions/DeploymentLogs"
        }
      }
    },
    "FlakeFile": {
      "type": "object",
      "properties": {
        "content": {
          "type": "string"
        },
        "id": {
          "type": "string"
        },
        "path": {
          "type": "string"
        }
      }
    },
    "FlakeInput": {
      "type": "object",
      "required": [
        "flakeURL"
      ],
      "properties": {
        "files": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FlakeFile"
          }
        },
        "flakeURL": {
          "type": "string"
        },
        "instanceType": {
          "type": "string"
        },
        "subdomainPrefix": {
          "type": "string"
        }
      }
    },
    "principal": {
      "type": "string"
    }
  },
  "securityDefinitions": {
    "key": {
      "type": "apiKey",
      "name": "x-token",
      "in": "header"
    }
  },
  "security": [
    {
      "key": []
    }
  ]
}`))
	FlatSwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "provide a nix flake and tigris provisions a compute environment and applies the flake to it",
    "title": "tigris",
    "version": "1.0.0"
  },
  "basePath": "/",
  "paths": {
    "/flake": {
      "post": {
        "description": "create a flake deployment",
        "summary": "create a flake deployment",
        "operationId": "flake",
        "parameters": [
          {
            "description": "flake url",
            "name": "flakeURL",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/FlakeInput"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "flake computed",
            "schema": {
              "$ref": "#/definitions/FlakeComputeResponse"
            }
          }
        }
      }
    },
    "/flake/{id}": {
      "get": {
        "description": "get a flake deployment",
        "summary": "get a flake deployment",
        "operationId": "getflake",
        "parameters": [
          {
            "type": "string",
            "description": "flake id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "flake computed",
            "schema": {
              "$ref": "#/definitions/FlakeComputeResponse"
            }
          }
        }
      },
      "delete": {
        "description": "delete a flake deployment",
        "summary": "delete a flake deployment",
        "operationId": "deleteflake",
        "parameters": [
          {
            "type": "string",
            "description": "flake id",
            "name": "id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "flake deleted",
            "schema": {
              "$ref": "#/definitions/FlakeCompute"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "DeploymentLogItem": {
      "type": "object",
      "properties": {
        "completed": {
          "type": "boolean"
        },
        "content": {
          "type": "string"
        },
        "disabled": {
          "type": "boolean"
        },
        "error": {
          "type": "boolean"
        },
        "icon": {
          "type": "string"
        },
        "label": {
          "type": "string"
        },
        "loading": {
          "type": "boolean"
        }
      }
    },
    "DeploymentLogs": {
      "type": "object",
      "properties": {
        "completed": {
          "type": "boolean"
        },
        "items": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DeploymentLogItem"
          }
        }
      }
    },
    "FlakeCompute": {
      "type": "object",
      "properties": {
        "domain": {
          "type": "string"
        },
        "flakeURL": {
          "type": "string"
        },
        "id": {
          "type": "string"
        },
        "instanceID": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "isTemplate": {
          "type": "boolean"
        },
        "logs": {
          "type": "string"
        },
        "status": {
          "type": "string"
        },
        "template": {
          "type": "string"
        }
      }
    },
    "FlakeComputeResponse": {
      "type": "object",
      "properties": {
        "flakeCompute": {
          "$ref": "#/definitions/FlakeCompute"
        },
        "logs": {
          "$ref": "#/definitions/DeploymentLogs"
        }
      }
    },
    "FlakeFile": {
      "type": "object",
      "properties": {
        "content": {
          "type": "string"
        },
        "id": {
          "type": "string"
        },
        "path": {
          "type": "string"
        }
      }
    },
    "FlakeInput": {
      "type": "object",
      "required": [
        "flakeURL"
      ],
      "properties": {
        "files": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FlakeFile"
          }
        },
        "flakeURL": {
          "type": "string"
        },
        "instanceType": {
          "type": "string"
        },
        "subdomainPrefix": {
          "type": "string"
        }
      }
    },
    "principal": {
      "type": "string"
    }
  },
  "securityDefinitions": {
    "key": {
      "type": "apiKey",
      "name": "x-token",
      "in": "header"
    }
  },
  "security": [
    {
      "key": []
    }
  ]
}`))
}
