// Code generated by go-swagger; DO NOT EDIT.

// Package restapi tigris
//
//	provide a nix flake and tigris provisions a compute environment and applies the flake to it
//	Schemes:
//	  http
//	Host: localhost
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	  - application/json
//
//	Produces:
//	  - application/json
//
// swagger:meta
package restapi
