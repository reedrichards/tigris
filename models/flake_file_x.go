package models

import (
	"encoding/base64"
	"pbin/cryptopasta"

	"github.com/pkg/errors"
)

func (m *FlakeFile) Encrypt(secretKey *[32]byte) error {
	encryptedContent, err := cryptopasta.Encrypt([]byte(m.Content), secretKey)
	if err != nil {
		return err
	}
	str := base64.StdEncoding.EncodeToString(encryptedContent)

	m.Content = str
	return nil
}

func (m *FlakeFile) Decrypt(secretKey *[32]byte) error {
	decoded, err := base64.StdEncoding.DecodeString(m.Content)
	if err != nil {
		return errors.Wrapf(err, "error decoding content %s", m.Content)
	}
	decryptedContent, err := cryptopasta.Decrypt(decoded, secretKey)
	if err != nil {
		return errors.Wrapf(err, "error decrypting content %s", m.Content)

	}
	m.Content = string(decryptedContent)
	return nil
}
