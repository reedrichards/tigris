package models

func (m *FlakeInput) GetInstanceType() *string {
	if m.InstanceType == "" {
		return nil
	}
	return &m.InstanceType
}
