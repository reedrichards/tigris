package main

import (
	"encoding/base64"
	"fmt"
	"pbin/cryptopasta"
)

func main() {
	key := cryptopasta.NewEncryptionKey()
	str := base64.StdEncoding.EncodeToString((*key)[:])
	fmt.Println(str)
}
