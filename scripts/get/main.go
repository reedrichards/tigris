package main

import (
	"context"
	"fmt"
	"pbin/client"
	"pbin/client/operations"
)

func main() {
	fmt.Println("Hello, world!")
	tc := client.NewHTTPClientWithConfig(
		nil,
		client.DefaultTransportConfig().WithHost("127.0.0.1:8080").WithSchemes([]string{"http"}),
	)

	resp, err := tc.Operations.Getflake(
		operations.NewGetflakeParamsWithContext(context.Background()).WithID(
			"73d49fb9-f0de-49f8-a03e-407657e31246",
		),
	)

	if err != nil {
		panic(err)
	}

	fmt.Println(resp.Payload)

}
