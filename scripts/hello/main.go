package main

import (
	"context"
	"fmt"
	"pbin/client"
	"pbin/client/operations"
	"pbin/models"
)

func main() {
	fmt.Println("Hello, world!")
	tc := client.NewHTTPClientWithConfig(
		nil,
		client.DefaultTransportConfig().WithHost("127.0.0.1:8080").WithSchemes([]string{"http"}),
	)

	s := "gitlab:reedrichards/aws-nixos#hello"

	resp, err := tc.Operations.Flake(
		operations.NewFlakeParamsWithContext(context.Background()).WithFlakeURL(
			&models.FlakeInput{
				FlakeURL: &s,
			},
		),
	)

	if err != nil {
		panic(err)
	}

	fmt.Println(resp.Payload)

}
